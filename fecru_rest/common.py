import requests
import config


def url(path):
    return config.url.rstrip('/') + "/rest-service-fecru/" + path.lstrip('/')

#paged result
def get_all_paged_results(paged_endpoint, start=0, limit=100):
    result=[]
    print "Getting paged: %s ..." % paged_endpoint
    while True:
        path = "%s?start=%d&limit=%d" % (paged_endpoint, start, limit)
        r = requests.get(url(path), auth=(config.user, config.password))
        assert(r.status_code == 200)
        data = r.json()
        result += data['values']
        if not data['lastPage']:
            start = start + data['limit']
        else:
            break
    return result

