import requests
import json
import config

from .common import *
from requests.exceptions import RequestException

# FishEye & Crucible example API for accessing /rest-service-fecru/admin/permission-schemes endpoint
# See https://docs.atlassian.com/fisheye-crucible/latest/wadl/fecru.html#rest-service-fecru:admin:permission-schemes for FeCru Permission Schemes REST API docs.

def get_permission_schemes(start=0, limit=100):
    return get_all_paged_results("admin/permission-schemes", start, limit)


def get_permission_scheme(name):
    path = "admin/permission-schemes/%s" % name
    r = requests.get(url(path), auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()


def create_permission_scheme(new_permission_scheme, copy_from_permission_scheme=None):
    if copy_from_permission_scheme is not None:
        path = "admin/permission-schemes?copyFrom=%s" % copy_from_permission_scheme
    else:
        path = "admin/permission-schemes"
    headers = {'content-type': 'application/json'}
    r = requests.post(url(path), data=json.dumps(new_permission_scheme), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 201)
    return r.json()


def update_permission_scheme(name, update_permission_scheme):
    path = "admin/permission-schemes/%s" % name
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(update_permission_scheme), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 200)
    return r.json()

def delete_permission_scheme(name):
    path = "admin/permission-schemes/%s" % (name)
    r = requests.delete(url(path), auth=(config.user, config.password))
    return r.status_code == 204

def try_delete_permission_scheme(name):
    try:
        delete_permission_scheme(name)
    except RequestException:
        pass


def __add_action_to_permission_scheme_sub_list(permission_scheme_name, name, action, sub_list):
    path = "admin/permission-schemes/%s/%s" % (permission_scheme_name, sub_list)
    payload = {'name': name, 'action' : action}
    headers = {'content-type': 'application/json'}
    r = requests.put(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    assert(r.status_code == 204)


def __delete_action_from_permission_scheme_sub_list(permission_scheme_name, name, action, sub_list):
    path = "admin/permission-schemes/%s/%s" % (permission_scheme_name, sub_list)
    payload = {'name': name, 'action' : action}
    headers = {'content-type': 'application/json'}
    r = requests.delete(url(path), data=json.dumps(payload), headers=headers, auth=(config.user, config.password))
    return r.status_code == 204


def __get_actions_from_permission_scheme_sub_list(permission_scheme_name, sub_list):
    path = "admin/permission-schemes/%s/%s" % (permission_scheme_name, sub_list)
    return get_all_paged_results(path)


def add_user_action(permission_scheme_name, user_name, action):
    return __add_action_to_permission_scheme_sub_list(permission_scheme_name, user_name, action, "users")


def delete_user_action(permission_scheme_name, user_name, action):
    return __delete_action_from_permission_scheme_sub_list(permission_scheme_name, user_name, action, "users")


def get_users_actions(permission_scheme_name):
    return __get_actions_from_permission_scheme_sub_list(permission_scheme_name, "users")


def add_group_action(permission_scheme_name, group_name, action):
    return __add_action_to_permission_scheme_sub_list(permission_scheme_name, group_name, action, "groups")


def delete_group_action(permission_scheme_name, group_name, action):
    return __delete_action_from_permission_scheme_sub_list(permission_scheme_name, group_name, action, "groups")


def get_groups_actions(permission_scheme_name):
    return __get_actions_from_permission_scheme_sub_list(permission_scheme_name, "groups")


def add_review_role_action(permission_scheme_name, review_role_name, action):
    return __add_action_to_permission_scheme_sub_list(permission_scheme_name, review_role_name, action, "review-roles")


def delete_review_role_action(permission_scheme_name, review_role_name, action):
    return __delete_action_from_permission_scheme_sub_list(permission_scheme_name, review_role_name, action, "review-roles")


def get_review_role_actions(permission_scheme_name):
    return __get_actions_from_permission_scheme_sub_list(permission_scheme_name, "review-roles")

def add_logged_in_users_action(permission_scheme_name, action):
    return __add_action_to_permission_scheme_sub_list(permission_scheme_name, None, action, "logged-in-users")


def delete_logged_in_users_action(permission_scheme_name, action):
    return __delete_action_from_permission_scheme_sub_list(permission_scheme_name, None, action, "logged-in-users")


def get_logged_in_users_actions(permission_scheme_name):
    return __get_actions_from_permission_scheme_sub_list(permission_scheme_name, "logged-in-users")


def add_anonymous_users_action(permission_scheme_name, action):
    return __add_action_to_permission_scheme_sub_list(permission_scheme_name, None, action, "anonymous-users")


def delete_anonymous_users_action(permission_scheme_name, action):
    return __delete_action_from_permission_scheme_sub_list(permission_scheme_name, None, action, "anonymous-users")


def get_anonymous_users_actions(permission_scheme_name):
    return __get_actions_from_permission_scheme_sub_list(permission_scheme_name, "anonymous-users")



